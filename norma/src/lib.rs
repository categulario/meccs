use levenshtein::levenshtein;

/// given a list of string and a single s
pub fn closest_file<'a>(files: &'a [String], title: &str) -> (&'a String, usize) {
    files.iter().map(|s| (s, levenshtein(s, title))).reduce(|acc@(_, d), item@(_, d1)| {
        if d1 < d {
            item
        } else {
            acc
        }
    }).unwrap()
}
