//! Make the json file from the csv that Jazmin fixed
use std::collections::HashSet;
use std::path::PathBuf;
use std::fs::{self, File};

use csvsc::prelude::*;
use serde::Serialize;
use clap::Parser;

use norma::closest_file;

#[derive(Serialize)]
struct Article {
    r#type: String,
    subject: String,
    title: String,
    author: String,
    year: String,
    lang: String,
    journal: String,
    size: String,
    url: String,
    filename: String,
}

struct UniqueCollector {
    r#type: HashSet<String>,
    subject: HashSet<String>,
    year: HashSet<String>,
    lang: HashSet<String>,
    journal: HashSet<String>,
}

impl UniqueCollector {
    fn new() -> UniqueCollector {
        UniqueCollector {
            r#type: HashSet::new(),
            subject: HashSet::new(),
            year: HashSet::new(),
            lang: HashSet::new(),
            journal: HashSet::new(),
        }
    }
}

#[derive(Serialize)]
struct Uniques {
    r#type: Vec<String>,
    subject: Vec<String>,
    year: Vec<String>,
    lang: Vec<String>,
    journal: Vec<String>,
}

impl From<UniqueCollector> for Uniques {
    fn from(collector: UniqueCollector) -> Uniques {
        let mut r#type: Vec<_> = collector.r#type.into_iter().filter(|s| !s.is_empty()).collect();
        let mut subject: Vec<_> = collector.subject.into_iter().filter(|s| !s.is_empty()).collect();
        let mut year: Vec<_> = collector.year.into_iter().filter(|s| !s.is_empty()).collect();
        let mut lang: Vec<_> = collector.lang.into_iter().filter(|s| !s.is_empty()).collect();
        let mut journal: Vec<_> = collector.journal.into_iter().filter(|s| !s.is_empty()).collect();

        r#type.sort_unstable();
        subject.sort_unstable();
        year.sort_unstable();
        lang.sort_unstable();
        journal.sort_unstable();

        Uniques {
            r#type, subject, year, lang, journal,
        }
    }
}

#[derive(Serialize)]
struct Data {
    articles: Vec<Article>,
    uniques: Uniques,
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Directory containing the files of the articles
    #[arg(long, value_name = "DIR")]
    files: PathBuf,

    /// File where json output will be stored
    #[arg(long, value_name = "FILE")]
    json_output: PathBuf,

    /// Input CSV file
    input: PathBuf,
}

fn main() {
    let args = Args::parse();
    let files: Vec<_> = fs::read_dir(&args.files).unwrap().map(|e| {
        e.unwrap().path().file_name().unwrap().to_str().unwrap().to_owned()
    }).collect();
    let mut uniques = UniqueCollector::new();
    let mut articles = Vec::new();

    let processed = InputStreamBuilder::from_paths([
            args.input,
        ]).unwrap().build().unwrap()

        .map_col("subject", |val| {
            Ok(val.replace("CO2", "CO₂").replace("Almacenamiento", "almacenamiento"))
        })

        .map_col("filename", |val| {
            if val.ends_with(".pdf") {
                Ok(val.to_string())
            } else {
                Ok(val.to_string().trim().to_owned() + ".pdf")
            }
        })

        .review(|headers, row| {
            uniques.r#type.insert(headers.get_field(row.as_ref().unwrap(), "type").unwrap().to_owned());
            uniques.subject.insert(headers.get_field(row.as_ref().unwrap(), "subject").unwrap().to_owned());
            uniques.year.insert(headers.get_field(row.as_ref().unwrap(), "year").unwrap().to_owned());
            uniques.lang.insert(headers.get_field(row.as_ref().unwrap(), "lang").unwrap().to_owned());
            uniques.journal.insert(headers.get_field(row.as_ref().unwrap(), "journal").unwrap().to_owned());
        })

        .del(vec!["corrected_filename", "match accuracy"])

        .map_row(|headers, row| {
            let title = headers.get_field(row, "title").unwrap();
            let filename = headers.get_field(row, "filename").unwrap();

            if args.files.join(filename).exists() {
                let mut new_row = row.clone();

                new_row.push_field(filename);
                new_row.push_field("-1");

                Ok(vec![
                   Ok(new_row)
                ].into_iter())
            } else {
                let mut new_row = row.clone();
                let (file, distance) = closest_file(&files, title);

                new_row.push_field(file);
                new_row.push_field(&distance.to_string());

                Ok(vec![
                   Ok(new_row)
                ].into_iter())
            }
        }, |headers| {
            let mut new_headers = headers.clone();

            new_headers.add("corrected filename");
            new_headers.add("match accuracy");

            new_headers
        })

        .flush(Target::stdout()).unwrap()

        .review(|headers, row| {
            if let Ok(row) = row {
                articles.push(Article {
                    r#type: headers.get_field(row, "type").unwrap().to_owned(),
                    subject: headers.get_field(row, "subject").unwrap().to_owned(),
                    title: headers.get_field(row, "title").unwrap().to_owned(),
                    author: headers.get_field(row, "author").unwrap().to_owned(),
                    year: headers.get_field(row, "year").unwrap().to_owned(),
                    lang: headers.get_field(row, "lang").unwrap().to_owned(),
                    journal: headers.get_field(row, "journal").unwrap().to_owned(),
                    size: headers.get_field(row, "size").unwrap().to_owned(),
                    url: headers.get_field(row, "url").unwrap().to_owned(),
                    filename: headers.get_field(row, "corrected filename").unwrap().to_owned(),
                });
            }
        })

        .into_iter()

        .count();

    let data = Data {
        articles, uniques: uniques.into(),
    };

    let json_file = File::create(args.json_output).unwrap();

    serde_json::to_writer_pretty(json_file, &data).unwrap();

    eprintln!("Processed {processed} articles");
}
