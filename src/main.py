#!/usr/bin/env python3
from os import path, listdir, environ
from shutil import copytree
from typing import Dict, Any
from pathlib import Path
import argparse
import json
import glob
import os

from jinja2 import (
    Environment, FileSystemLoader, select_autoescape, pass_context,
)
from jinja2.exceptions import TemplateNotFound

SOURCE_DIRECTORY = path.dirname(__file__)


class Config:
    PUBLIC_DIRECTORY: str = path.normpath(path.join(SOURCE_DIRECTORY, '../public'))
    DATA_FILE: str = path.normpath(path.join(SOURCE_DIRECTORY, 'data.json'))
    I18N_STRINGS: str = path.normpath(path.join(SOURCE_DIRECTORY, 'i18n.json'))


def read_data(path: Path) -> Dict[str, Any]:
    try:
        with open(path) as datafile:
            return json.load(datafile)
    except FileNotFoundError:
        return {}


# Some filters
@pass_context
def i18n(ctx, string):
    return ctx['i18n_strings'][ctx['lang']][string]


def build(config: Config):
    context = read_data(config.DATA_FILE)
    i18n_strings = read_data(config.I18N_STRINGS)
    env = Environment(
        loader=FileSystemLoader(path.join(SOURCE_DIRECTORY, 'templates')),
        autoescape=select_autoescape()
    )

    env.filters['i18n'] = i18n

    # render the pages
    pages_dir = path.join(SOURCE_DIRECTORY, 'templates/pages/')

    for root, dirs, files in os.walk(pages_dir):
        for file in files:
            template = path.join(root[len(pages_dir):], file)
            index = env.get_template(f"pages/{template}")

            (Path(config.PUBLIC_DIRECTORY) / template).parent.mkdir(parents=True, exist_ok=True)

            with open(path.join(config.PUBLIC_DIRECTORY, template), 'w') as indexfile:
                indexfile.write(index.render({
                    'current_path': template,
                    'i18n_strings': i18n_strings,
                    **context
                }))

    # copy static files
    copytree(
        path.join(SOURCE_DIRECTORY, 'static'),
        path.join(config.PUBLIC_DIRECTORY, 'static'),
        dirs_exist_ok=True,
    )

    print(f'Built for production at {config.PUBLIC_DIRECTORY}')


def development_server(config: Config):
    context = read_data(config.DATA_FILE)
    i18n_strings = read_data(config.I18N_STRINGS)
    from flask import Flask, render_template

    app = Flask(__name__)

    app.jinja_env.filters['i18n'] = i18n

    @app.route("/")
    def index():
        return render_template('pages/index.html', **{
            'current_path': '/',
            **context
        })

    @app.route("/<path:pagepath>")
    def pages(pagepath):
        template_path = Path(SOURCE_DIRECTORY) / 'templates/pages' / pagepath
        index_path = Path(SOURCE_DIRECTORY) / 'templates/pages' / pagepath / 'index.html'

        local_context = {
            'current_path': pagepath,
            'i18n_strings': i18n_strings,
            **context
        }

        try:
            if template_path.is_file():
                return render_template(f'pages/{pagepath}', **local_context)
            elif template_path.is_dir() and index_path.is_file():
                return render_template(f'pages/{pagepath}/index.html', **local_context)
            else:
                return f'Template not found: {pagepath}', 404
        except TemplateNotFound as e:
            return f'Template not found: {e}', 404

    app.run(host='127.0.0.1', port=int(environ.get('MECCS_PORT', '8000')), debug=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Build a static site')

    parser.add_argument(
        '--dev', action='store_true', help='run the development server',
    )

    args = parser.parse_args()

    if args.dev:
        development_server(Config())
    else:
        build(Config())
