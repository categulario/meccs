/**
 * Change the style of the navbar due to scrolling
 */
function restyleNav() {
  if (window.scrollY > 0) {
    document.getElementById('main-nav').classList.add('scroll');
  } else {
    document.getElementById('main-nav').classList.remove('scroll');
  }
}

function findParent(element, selector) {
  while (true) {
    if (element.matches(selector)) {
      return element;
    }

    element = element.parentNode;

    if (element === null) {
      break;
    }
  }

  return null;
}

document.addEventListener('DOMContentLoaded', () => {
  restyleNav();

  window.addEventListener('scroll', restyleNav);

  const nav = document.getElementById('main-nav');

  nav.addEventListener('click', (event) => {
    console.log(event.target);
    console.log(event.currentTarget);

    if (event.target.matches('#toggle *')) {
      event.preventDefault();
    }

    if (event.target.matches('.dropdown a') || event.target.matches('#toggle *')) {
      const main_nav = document.getElementById('main-nav');

      main_nav.classList.toggle('open');
    }
  });
});
