function getTemplate(name) {
  let template = document.getElementById(name);
  let templateContent = template.content;

  return templateContent.cloneNode(true);
}
