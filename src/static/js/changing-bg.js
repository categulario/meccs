function get_new_front_back(event) {
  if (event.target.id === 'bg-a') {
    return [document.getElementById('bg-b'), event.target];
  } else {
    return [document.getElementById('bg-a'), event.target];
  }
}

document.addEventListener('DOMContentLoaded', () => {
  let cur_front = 2;

  function animationFinished(element) {
    const [new_front, new_back] = get_new_front_back(element);

    const back_bg = cur_front;
    cur_front = cur_front % 3 + 1;

    new_back.className = `bg bg-${back_bg} bg-back`;
    new_front.className = `bg bg-${cur_front} bg-front bg-animated`;
  }

  document.getElementById('bg-b').addEventListener('animationend', animationFinished);
  document.getElementById('bg-a').addEventListener('animationend', animationFinished);
});
