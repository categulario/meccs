class QuestionLoop extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({mode: 'open'}).appendChild(getTemplate('question-loop'));

    // useful nodes
    this.questionText = this.shadowRoot.querySelector('[question-text]');
    this.questionNum = this.shadowRoot.querySelector('[question-num]');
    this.questionInfo = this.shadowRoot.querySelector('[question-info]');
    this.questionImage = this.shadowRoot.querySelector('[question-image]');
    this.questionQuestion = this.shadowRoot.querySelector('[question-question]');
    this.questionNext = this.shadowRoot.querySelector('[question-next]');
    this.questionOptions = this.shadowRoot.querySelector('[question-options]');
    this.nextBtn = this.shadowRoot.querySelector('[next-btn]');
    this.progress = this.shadowRoot.querySelector('.progress-bar');

    // methods that require this rebinding
    this.optionClicked = this.optionClicked.bind(this);
    this.nextClicked = this.nextClicked.bind(this);

    this.options = [];
  }

  connectedCallback() {
    this.poll = this.getAttribute('poll');
    this.questionData = JSON.parse(document.getElementById(`${this.poll}-poll-data`).text);

    this.nextBtn.addEventListener('click', this.nextClicked);
    this.loadQuestion(+this.question);
  }

  disconnectedCallback() {
    this.unloadQuestion();
    this.nextBtn.removeEventListener('click', this.nextClicked);
  }

  static get observedAttributes() {
    return ['question'];
  }

  attributeChangedCallback(name, _oldValue, newValue) {
    if (name == 'question') {
      this.loadQuestion(+newValue);
    }
  }

  // methods

  loadQuestion(index) {
    if (this.questionData === undefined) {
      return;
    }

    const qData = this.questionData.questions[index];
    const total = this.questionData.questions.length;

    this.questionNum.innerText = index + 1;
    this.questionInfo.innerText = qData.info;
    this.questionImage.src = qData.image;
    this.questionText.innerText = qData.question;
    this.progress.style.width = `${index / total * 100}%`;

    if (this.getAttribute('lang') === 'es') {
      this.questionQuestion.innerText = 'Pregunta';
      this.questionNext.innerText = 'Siguiente';
    } else {
      this.questionQuestion.innerText = 'Question';
      this.questionNext.innerText = 'Next';
    }

    this.questionOptions.innerHTML = "";

    this.options = qData.options.map((option) => {
      const optTag = document.createElement('question-option');

      optTag.setAttribute('value', option.id);
      optTag.setAttribute('label', option.label);

      optTag.addEventListener('click', this.optionClicked);

      this.questionOptions.appendChild(optTag);

      return optTag;
    });
    this.resetOptionClicked();
  }

  unloadQuestion() {
    this.options.forEach((opt) => {
      opt.removeEventListener('click', this.optionClicked);
    });

    this.options = [];
  }

  setOptionClicked() {
    this.setAttribute("clicked", "clicked");
  }

  resetOptionClicked() {
    this.setAttribute("clicked", "");
  }

  // event handlers

  optionClicked(event) {
    event.target.setAttribute('checked', true);

    let opts = Array.from(this.shadowRoot.querySelectorAll('question-option'));

    opts.forEach((opt) => {
      if (opt == event.target) {
        opt.setAttribute('checked', true);
      } else {
        opt.setAttribute('checked', false);
      }
    });

    this.setOptionClicked();

    this.questionData.questions[+this.question].userAnswer = event.target.getAttribute('value');
  }

  nextClicked(event) {
    event.preventDefault();

    const nextQuestion = +this.question + 1;

    if (nextQuestion < this.questionData.questions.length) {
      this.question = nextQuestion;
    } else {
      const event = new CustomEvent('poll-finished', {
        detail: this.questionData,
      });

      this.dispatchEvent(event);
    }
  }

  // getters and setters

  get question() {
    return this.getAttribute('question');
  }

  set question(newValue) {
    this.setAttribute('question', newValue);
  }
}

customElements.define('question-loop', QuestionLoop);
