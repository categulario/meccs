class QuestionOptionResult extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({mode: 'open'}).appendChild(getTemplate('question-option-result'));

    this.labelContainer = this.shadowRoot.querySelector(`[label]`);
  }

  connectedCallback() {
  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return ['label'];
  }

  attributeChangedCallback(_name, _oldValue, newValue) {
    this.labelContainer.innerText = newValue;
  }
}

customElements.define('question-option-result', QuestionOptionResult);
