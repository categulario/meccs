const TEXT_CONTAINERS = ['title', 'info', 'question'];
const HTML_CONTAINERS = ['answerinfo'];
const IMG_CONTAINERS = ['image'];
const ALL_CONTAINERS = TEXT_CONTAINERS.concat(HTML_CONTAINERS, IMG_CONTAINERS, ['options']);

class QuestionResult extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({mode: 'open'}).appendChild(getTemplate('question-result'));

    for (const name of ALL_CONTAINERS) {
      this[`${name}Container`] = this.shadowRoot.querySelector(`[${name}]`);
    }
  }

  connectedCallback() {
  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return [
      'id', 'poll', 'title', 'answer', 'image', 'info', 'options', 'question',
      'useranswer',
    ];
  }

  attributeChangedCallback(name, _oldValue, newValue) {
    if (name === 'id') {
      this.answerinfoContainer.innerHtml = '';
      this.answerinfoContainer.appendChild(getTemplate(`${this.poll}-${this.id}-answer`));
    } else if (IMG_CONTAINERS.includes(name)) {
      this[`${name}Container`].src = newValue;
    } else if (TEXT_CONTAINERS.includes(name)) {
      this[`${name}Container`].innerText = newValue;
    } else if (name === 'options') {
      for (const answer of JSON.parse(newValue)) {
        const option = document.createElement('question-option-result');

        option.setAttribute('label', answer.label);
        option.setAttribute('rightanswer', answer.id === this.answer ? 'true': 'false');
        option.setAttribute('useranswer', answer.id === this.useranswer ? 'true': 'false');

        this.optionsContainer.appendChild(option);
      }
    }
  }

  // getters and setters
  get answer() {
    return this.getAttribute('answer');
  }

  set answer(value) {
    this.setAttribute('answer', value);
  }

  get useranswer() {
    return this.getAttribute('useranswer');
  }

  set useranswer(value) {
    this.setAttribute('useranswer', value);
  }

  get poll() {
    return this.getAttribute('poll');
  }

  set poll(value) {
    this.setAttribute('poll', value);
  }

  get id() {
    return this.getAttribute('id');
  }

  set id(value) {
    this.setAttribute('id', value);
  }
}

customElements.define('question-result', QuestionResult);
