class PollResults extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({mode: 'open'}).appendChild(getTemplate('poll-results'));

    this.answerContainer = this.shadowRoot.querySelector('[answer-container]');
    this.linkBack = this.shadowRoot.querySelector('[link-back]');

    this.goBack = this.goBack.bind(this);
  }

  connectedCallback() {
    this.linkBack.addEventListener('click', this.goBack);
  }

  disconnectedCallback() {
    this.linkBack.removeEventListener('click', this.goBack);
  }

  static get observedAttributes() {
    return ['data'];
  }

  attributeChangedCallback(_name, _oldValue, newValue) {
    this.data = JSON.parse(newValue);

    this.render();
  }

  goBack(event) {
    event.preventDefault();

    this.dispatchEvent(new Event('go-back'));
  }

  render() {
    for (let i = 0; i < this.data.questions.length; i++) {
      const question = this.data.questions[i];
      const questionResult = document.createElement('question-result');

      questionResult.setAttribute('poll', this.data.poll);
      questionResult.setAttribute('id', i + 1);
      if (this.getAttribute('lang') === 'es') {
        questionResult.setAttribute('title', `Respuesta ${i + 1}`);
      } else {
        questionResult.setAttribute('title', `Answer ${i + 1}`);
      }
      questionResult.setAttribute('answer', question.answer);
      questionResult.setAttribute('image', question.image);
      questionResult.setAttribute('info', question.info);
      questionResult.setAttribute('question', question.question);
      questionResult.setAttribute('useranswer', question.userAnswer);
      questionResult.setAttribute('options', JSON.stringify(question.options));

      this.answerContainer.appendChild(questionResult);
    }
  }
}

customElements.define('poll-results', PollResults);
