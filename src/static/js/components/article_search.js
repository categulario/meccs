function makeOption(value, text) {
  const option = document.createElement('option');

  option.value = value;
  option.innerText = text;

  return option;
}

const SELECT_FIELDS = ['journal', 'type', 'subject', 'year', 'lang'];
const ALL_FIELDS = SELECT_FIELDS.concat(['q']);

class ArticleSearch extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({mode: 'open'}).appendChild(getTemplate('article-search'));

    this.searchInput = this.shadowRoot.querySelector('[search-input]');
    this.clearSearchBtn = this.shadowRoot.querySelector('[clear-search]');

    for (const attr of SELECT_FIELDS) {
      this[`${attr}Select`] = this.shadowRoot.querySelector(`select[name="${attr}"]`);
    }

    this.searchResults = this.shadowRoot.querySelector('search-results');

    this.selectChanged = this.selectChanged.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
  }

  connectedCallback() {
    this.loadData();

    this.searchInput.addEventListener('keyup', this.selectChanged);

    for (const attr of SELECT_FIELDS) {
      this[`${attr}Select`].addEventListener('change', this.selectChanged);
    }

    this.clearSearchBtn.addEventListener('click', this.clearSearch);
  }

  disconnectedCallback() {
    this.searchInput.removeEventListener('keyup', this.selectChanged);

    for (const attr of SELECT_FIELDS) {
      this[`${attr}Select`].removeEventListener('change', this.selectChanged);
    }

    this.clearSearchBtn.removeEventListener('click', this.clearSearch);
  }

  // attributes
  get hasSearch() {
    return this.getAttribute('has-search');
  }

  set hasSearch(value) {
    return this.setAttribute('has-search', value);
  }

  // listeners
  selectChanged(event) {
    this.searchResults.setAttribute(event.target.name, event.target.value);

    if (event.target.value) {
      this.hasSearch = 'true';
    } else if (this.noSearchMade()) {
      this.hasSearch = '';
    }

    window.scrollTo(0, this.offsetTop -50);
  }

  // methods
  /**
   * returns true if all the selects and the search input are empty (no search
   * active)
   */
  noSearchMade() {
    for (const attr of ALL_FIELDS) {
      if (this.searchResults.getAttribute(attr)) {
        return false;
      }
    }

    return true;
  }

  clearSearch() {
    // unset all selects
    for (const attr of SELECT_FIELDS) {
      this[`${attr}Select`].value = '';
    }

    // clear search input
    this.searchInput.value = '';

    // reset attributes in search results to clear the result list
    for (const attr of ALL_FIELDS) {
      this.searchResults.setAttribute(attr, '');
    }

    // clean this element's has-search attribute
    this.hasSearch = '';
  }

  loadData() {
    (async () => {
      const req = await fetch("/static/repo.json");
      const data = await req.json();

      this.searchResults.setAttribute('articles', JSON.stringify(data.articles));

      this.uniques = data.uniques;

      this.reloadSelects();
    })();
  }

  reloadSelects() {
    for (const [key, value] of Object.entries(this.uniques)) {
      const select = this[`${key}Select`];

      // remove all children but the first (empty) option
      let children = Array.from(select.children);
      for (let i = 0; i < children.length; i++) {
        if (i > 0) {
          select.removeChild(children[i]);
        }
      }

      // add options
      for (const opt of value) {
        select.appendChild(makeOption(opt, opt));
      }

      select.value = '';
    }
  }
}

customElements.define('article-search', ArticleSearch);

console.log(`Heeeeeeeeeeeeey! Así que eres programadora? (o programador)');

Yo también. No sé qué veniste a buscar a la consola, pero si de pura casualidad
lo que querías es la lista completa de artículos de este sitio la puedes
encontrar en:

[english] So you are a programmer? So am I! I don't know what you where looking
for here, but if it was the article list to download you can find it at:

${location.protocol}//${location.host}/static/repo.json

Happy hacking!

Atte. El @categulario`);
