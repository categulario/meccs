class SearchResults extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({mode: 'open'}).appendChild(getTemplate('search-results'));

    this.articles = [];
  }

  connectedCallback() {
  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return [
      'articles', 'q', 'type', 'subject', 'year', 'lang', 'journal',
    ];
  }

  attributeChangedCallback(name, _oldValue, newValue) {
    if (name === 'articles') {
      this.articles = JSON.parse(newValue);
    }

    this.refreshList();
  }

  filterArticle(article) {
    const query = this.getAttribute('q');
    const type = this.getAttribute('type');
    const subject = this.getAttribute('subject');
    const year = this.getAttribute('year');
    const lang = this.getAttribute('lang');
    const journal = this.getAttribute('journal');

    if (!query && !type && !subject && !year && !lang && !journal) {
      // no filtering or anything, don't show any article
      return false;
    }

    const regex = new RegExp(query, 'i');
    if (query && !regex.test(article.title) && !regex.test(article.author)) {
      return false;
    }

    if (type && article.type !== type) return false;
    if (subject && article.subject !== subject) return false;
    if (year && article.year !== year) return false;
    if (lang && article.lang !== lang) return false;
    if (journal && article.journal !== journal) return false;

    return true;
  }

  refreshList() {
    this.shadowRoot.innerHTML = "";

    const shownArticles = this.articles.filter(this.filterArticle.bind(this));

    for (let article of shownArticles) {
      const el = document.createElement('article-entry');

      for (let prop of ['type', 'subject', 'title', 'author', 'year', 'lang', 'journal', 'url', 'filename']) {
        el.setAttribute(prop, article[prop]);
      }

      this.shadowRoot.appendChild(el);
    }
  }
}

customElements.define('search-results', SearchResults);
