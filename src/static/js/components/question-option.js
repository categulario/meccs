class QuestionOption extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({mode: 'open'}).appendChild(getTemplate('question-option'));

    this.label = this.shadowRoot.querySelector('.label-text');
  }

  connectedCallback() { }

  disconnectedCallback() { }

  static get observedAttributes() {
    return ['label'];
  }

  attributeChangedCallback(name, _oldValue, newValue) {
    if (name == 'label') {
      this.label.innerText = newValue;
    }
  }
}

customElements.define('question-option', QuestionOption);
