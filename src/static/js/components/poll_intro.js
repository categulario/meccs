class PollIntro extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({mode: 'open'}).appendChild(getTemplate('poll-intro'));

    this.carbonLink = this.shadowRoot.querySelector('[data-poll="ciclodelcarbono"]');
    this.sustainabilityLink = this.shadowRoot.querySelector('[data-poll="sostenibilidad"]');

    this.startPoll = this.startPoll.bind(this);
  }

  connectedCallback() {
    this.carbonLink.addEventListener('click', this.startPoll);
    this.sustainabilityLink.addEventListener('click', this.startPoll);
  }

  disconnectedCallback() {
    this.carbonLink.removeEventListener('click', this.startPoll);
    this.sustainabilityLink.removeEventListener('click', this.startPoll);
  }

  startPoll(event) {
    event.preventDefault();

    const startPollEvent = new CustomEvent('start-poll', {
      detail: event.target.dataset.poll,
    });

    this.dispatchEvent(startPollEvent);
  }
}

customElements.define('poll-intro', PollIntro);
