class ArticleEntry extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({mode: 'open'}).appendChild(getTemplate('article-entry'));

    for (let attr of ArticleEntry.observedAttributes) {
      this[`${attr}Container`] = this.shadowRoot.querySelector(`[${attr}]`);
    }
  }

  connectedCallback() {
  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return [
      'type', 'subject', 'title', 'author', 'year', 'lang', 'journal', 'filename',
    ];
  }

  attributeChangedCallback(name, _oldValue, newValue) {
    if (name === 'filename') {
      this.filenameContainer.href = `/static/repo/${encodeURIComponent(newValue)}`;
    } else {
      this[`${name}Container`].innerText = newValue;
    }
  }
}

customElements.define('article-entry', ArticleEntry);
