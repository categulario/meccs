class PollApp extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({mode: 'open'}).appendChild(getTemplate('poll-app'));

    this.containerDiv = this.shadowRoot.querySelector('[component]');
    this.introComponent = this.shadowRoot.querySelector('poll-intro');

    this.pollFinished = this.pollFinished.bind(this);
    this.startPoll = this.startPoll.bind(this);
    this.goBack = this.goBack.bind(this);
  }

  connectedCallback() {
    this.introComponent.addEventListener('start-poll', this.startPoll);
  }

  disconnectedCallback() {
    this.introComponent.removeEventListener('start-poll', this.startPoll);
  }

  startPoll(event) {
    const poll = event.detail;

    const questionLoop = document.createElement('question-loop');

    questionLoop.setAttribute('poll', poll);
    questionLoop.setAttribute('lang', this.getAttribute('lang'));
    questionLoop.addEventListener('poll-finished', this.pollFinished);

    this.containerDiv.innerHTML = '';
    this.containerDiv.appendChild(questionLoop);
  }

  pollFinished(event) {
    const pollResults = document.createElement('poll-results');

    pollResults.setAttribute('lang', this.getAttribute('lang'));
    pollResults.setAttribute('data', JSON.stringify(event.detail));

    // Is this a memory leak? (since I'm not cleaning the handler)
    pollResults.addEventListener('go-back', this.goBack);

    this.containerDiv.innerHTML = '';
    this.containerDiv.appendChild(pollResults);
  }

  goBack() {
    this.containerDiv.innerHTML = '';
    this.containerDiv.appendChild(this.introComponent);

    window.location.hash = '#how-much-you-know';
  }
}

customElements.define('poll-app', PollApp);
