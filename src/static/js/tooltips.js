function addTooltips () {
  const els = document.querySelectorAll(".info-item");
  const data = JSON.parse(document.getElementById('svg-data').innerHTML);

  for (let index = 0; index < els.length; index++) {
    const element = els[index];
    const elData = data[element.dataset.item];

    // complete tooltip, includes info
    const label = `<div class="tooltip ${elData.type}">
  <div class="tooltip-head">${elData.header}</div>
  <div class="tooltip-body">${elData.body}</div>
</div>`;
    setTooltip(element, label, {
      "hideOnClick": true,
      "trigger": 'mouseenter focus'
    });
  }
}

function setTooltip (element, label, opt = {
  "hideOnClick": 'toggle',
  "trigger": 'click'
}) {
  tippy('#' + element.id, {
    content: label,
    hideOnClick: opt.hideOnClick,
    trigger: opt.trigger,
    allowHTML: true,
    animation: 'scale',
    zIndex: 9,
    theme: 'meccs'
  });
}

Array.from(document.getElementsByClassName('legend__text')).forEach((el) => {
  el.addEventListener('mouseenter', () => {
    Array.from(document.querySelectorAll(`[data-class="${el.dataset.category}"]`)).forEach((item) => {
      item.classList.add('reveal');
    });
  });
  el.addEventListener('mouseleave', () => {
    Array.from(document.querySelectorAll(`[data-class="${el.dataset.category}"]`)).forEach((item) => {
      item.classList.remove('reveal');
    });
  });
});

(addTooltips());
