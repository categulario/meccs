const ESC_KEY = "Escape";

function closeModal(event) {
  event.preventDefault();
  document.body.classList.remove('has-modal');

  // the next is to stop the video if playing
  const videoIframe = document.getElementById('video-frame');
  const src = videoIframe.src;
  videoIframe.src = src;
}

function openModal(videoId) {
  document.getElementById('video-frame').src = `https://www.youtube-nocookie.com/embed/${videoId}`;
  document.body.classList.add('has-modal');
}

document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('.video-play-btn').forEach((el) => {
    el.addEventListener('click', (event) => {
      event.preventDefault();

      openModal(el.dataset.video);
    });
  });

  // handle esc to close vide overlay
  document.addEventListener('keydown', (event) => {
    if (event.code === ESC_KEY) {
      closeModal(event);
    }
  });
  const overlay = document.getElementById('video-overlay');
  overlay.addEventListener('click', closeModal);
});
