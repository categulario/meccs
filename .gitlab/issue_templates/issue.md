Tengo una sugerencia
===

Sección
---

¿En qué sección encontraste el problema/área de oportunidad? Las secciones son
(Inicio, CCS y Sostenibilidad, CCS en México, Investigación, Quienes
participamos y Contacto)

Detalle
---

Pon aquí lo que encontraste, y cómo podría ser mejor

Captura de pantalla
---

Si es un detalle visual, adjunta una captura de pantalla editada en paint donde
se señale el problema que encontraste
