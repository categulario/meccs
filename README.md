# Sitio web MeCCS

## Cómo desarrollar

Asumiendo que estás dentro de la carpeta del proyecto hay que hacer las
siguientes cosas:

Crea un entorno virtual:

    virtualenv .venv

Actívalo:

    source .venv/bin/activate

Instala las dependencias:

    pip install -r requirements.txt

Y corre el sitio en modo desarrollo:

    python src/main.py --dev

O compílalo para producción:

    python src/main.py

## Base de datos de artículos

Hay que correr el programa en la carpeta `norma` contra un archivo fuente csv
con información de los artículos como sigue:

    cd norma
    cargo run --release -- --files repo --json-output repo.json repo.csv > repo-clean.csv

esto generará un archivo csv llamado `repo-clean.csv` con los datos limpios y
normalizados y otro archivo `repo.json` con la misma información pero en formato
json.

## Subir una nueva versión de la gaceta

1. Añadir una entrada a la lista `gacetas` de `src/data.json` por ahí de la
   línea 1532.
2. Añade la portada de la imagen con un ancho de 500 píxeles a
   `src/static/img/gaceta/` con el nombre correcto.
